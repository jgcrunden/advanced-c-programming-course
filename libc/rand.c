#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void) {


    printf("RAND\n");
    for(int i =0; i < 5; i++) {
        printf("%d\n", rand());
    }
    printf("\n\n SRAND\n");
    srand(time(0));
    for(int i = 0; i < 10; i++) {
        printf("%d\n", rand() %5 +1);
    }
    return 0;
}