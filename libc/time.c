#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
    time_t calendar_start = time(NULL);
    clock_t clock_start = clock();
    // char time_str[30];
    time_t calendar = time(NULL);
    printf("Date is %s", ctime(&calendar));

    struct tm *time_data;
    time_data = localtime(&calendar);
    printf("Today is %d %d %d\n", time_data->tm_year +1900, time_data->tm_mon, time_data->tm_mday);

    clock_t clock_end = clock();
    time_t calendar_end = time(NULL);
    printf("CPU time %lf\n", ((double)(clock_end-clock_start))/CLOCKS_PER_SEC);
    printf("calendar time %lf\n", difftime(calendar_end, calendar_start));
    return 0;
}