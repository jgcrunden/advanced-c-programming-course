#include <stdlib.h>
#include <stdio.h>

int main(void) {
    char a[10] = "100";
    int value = atoi(a);
    printf("%d\n", value);
    
    char b[10] = "3.14";
    float value2 = atof(b);
    printf("Value = %.2f\n", value2);

    char c[17] = "1000000000000000";
    long value3 = atol(c);
    printf("Value = %ld\n", value3);

    char d[20];
    sprintf(d, "%d", 45);
    printf("%s\n", d);

    char e[] = "123.4567xyz", *end;
    double value4 = 0;
    value4 = strtod(e, &end);
    printf("%lf\n", value4);

    char f[] = "365.25 7.0", *end2;
    float value5= 0;
    value5 = strtof(f, &end2);
    float value6 = strtof(end2, NULL);
    printf("%lf\n", value5);
    printf("%lf\n", value6);

    char g[] = "2030303030 This is a test";
    char *ptr = NULL;
    long ret = 0;
    ret = strtol(g, &ptr, 10);
    printf("The nuber (unsigned long integer) is &ld\n", ret);
    printf("String par is [%s]\n", ptr);

    return 0;
}