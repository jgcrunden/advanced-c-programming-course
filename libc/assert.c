/*#define NDEBUG To ignore asserts */
#include <assert.h>
#include <stdio.h>
#include <limits.h>
#include <stdbool.h>
// #pragma message "Computer says no!"

static_assert(CHAR_BIT == 16, "16-bit char falsely assumed");

int main(void) {
    int y =5;
    for(int x  = 0; x < 20; ++x) {
        printf("x = %d y = %d\n", x, y);
        assert(x < y);
    }
    return 0;
}