#include <stdio.h>

int main() {
    char *str = "Joshua C 29";
    char name[10], surname[10];
    int age = 0, ret = 0;

    ret = sscanf(str, "%s %s %d", name, surname, &age);

    printf("Name: %s\n", name);
    printf("Surname: %s\n", surname);
    printf("Age: %d\n", age);

    return 0;
}