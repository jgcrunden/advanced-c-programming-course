#include <stdio.h>
#include <stdlib.h>

int main() {

    FILE *f = NULL;
    char ch[100];

    if ((f = fopen("data.txt", "r+")) == NULL) {
        printf("Cannot open this file\n");
        exit(1);
    }

    for (int i = 0; i < 10; i++) {
        fprintf(f, "This count number is %d\n", i+1);
    }

    fclose(f);

     if ((f = fopen("data.txt", "r+")) == NULL) {
        printf("Cannot open this file\n");
        exit(1);
    }

    printf("File contents is...\n");
    while(!feof(f)) {
        fgets(ch, 100, f);
        printf("%s", ch);
    }
}