#include <stdio.h>
#include <string.h>

int main() {
    char buff[255];
    int ch = '\0';
    char *p = NULL;

    if (fgets(buff, sizeof(buff), stdin)) {
        p = strchr(buff, '\n');
        if(p) {
            *p = '\0';
        } else {
            while (((ch = getchar()) != '\n') && !feof(stdin) && !ferror(stdin)) {
                printf("Doing nothing\n");
            }
        }
    }

    printf("%s\n", buff);
    return 0;
}