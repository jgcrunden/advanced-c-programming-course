#include <stdio.h>

int main() {
    int ch = '\0';
    FILE *fp;
    if(fp = fopen("test.txt", "rw")) {
        while((ch = getc(fp)) != EOF)
            printf("%c", ch);
        fclose(fp);
    }
    printf("\n");

    while((ch = getchar()) != EOF)
        printf("%c", ch);

    printf("\n");

    if(fp = fopen("test.txt", "rw")) {
        while((ch = fgetc(fp)) != EOF)
            printf("%c", ch);
        fclose(fp);
        printf("\n");
    }

    return 0;
}