#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main() {

    FILE *fp = NULL;
    fp = fopen("somefile.txt", "w");
    if (fp == NULL) {
        exit(1);
    }

    fputs("Hello there, my name is Josh", fp);

    fclose(fp);
    return 0;
}