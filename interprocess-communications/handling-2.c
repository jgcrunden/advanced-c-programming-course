#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>

void signalHandler(int signalValue);

int main(void) {
    int x;
    int i;
    signal(SIGINT, signalHandler);
    
    srand(clock());

    for(i = 0; i < 100; i++) {
        x = 1 + rand() % 50;
        if(x == 25)
            raise(SIGINT);
        
        printf("%4d", i);
        if (i % 10 == 0)
            printf("\n");
    }

   

    return 0;
}

void signalHandler(int signalValue) {
    int response;

    printf("Interrupted signal (%d), Do you wish to continue?", signalValue);
    scanf("%d", &response);

    while(response != 1 && response != 2) {
        printf("(1 = yes or 2 = no )? ");
        scanf("%d", &response);
    }

    if(response == 1) {
        signal(SIGINT, signalHandler);
    } else {
        exit(EXIT_SUCCESS);
    }
}