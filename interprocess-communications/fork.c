#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>

#define MAX_COUNT 10
#define BUF_SIZE 100

int main(void) {
    pid_t pid;
    char buff[BUF_SIZE];

    fork();

    pid = getpid();
    for(int i = 1; i < MAX_COUNT; i++) {
        sprintf(buff, "This line is from pid %d, value = %d\n", pid, i);
        write(1, buff, strlen(buff));
    }

    printf("Hello world\n");
    return 0;
}