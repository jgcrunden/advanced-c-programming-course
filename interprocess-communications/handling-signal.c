#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

void handlerDivideByZero(int signum);



int main(void) {
    int result = 0;
    int v1 = 0, v2 = 0;
    v1 = 121;

    void (*sigHandlerReturn)(int);

    sigHandlerReturn =signal(SIGFPE, handlerDivideByZero);

    if(sigHandlerReturn == SIG_ERR) {
        perror("Signal error: ");
        return 1;
    }

    result = v1 /v2;
    printf("Result of v1 / v2 is %d\n", result);
    return 0;
}

void handlerDivideByZero(int signum) {
    if(signum == SIGFPE) {
        printf("Received SIGFPE, Divided by Zero Exception\n");
        exit(0);
    } else {
        printf("Received %d signal\n", signum);
        return;
    }
}