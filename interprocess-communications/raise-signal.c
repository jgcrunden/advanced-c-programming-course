#include <stdio.h>
#include <signal.h>

int main (void) {
    printf("Testing SIGSTOP\n");
    raise(SIGSTOP);
    printf("Continuing with programme\n");
    return 0;
}