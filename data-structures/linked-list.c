#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    char data;
    struct node *next;
} node_t;

typedef node_t *ListNodePtr;
/* prototypes */
void insert(ListNodePtr *head, char value);
void insertAtEnd(ListNodePtr *head, char value);
void insertAtBeginning(ListNodePtr *head, char value);
char delete(ListNodePtr *head, char value);
void deleteAtBeginning(ListNodePtr *head);
int isEmpty(ListNodePtr head);
void printList(ListNodePtr currentPtr);

int main() {
    ListNodePtr head = NULL;
    int choice = 0;
    char item = '\0';

    insertAtBeginning(&head, 'a');
    insert(&head, 'b');
    printf("%d\n",isEmpty(head));
    insertAtEnd(&head, 'c');

    printList(head);
    delete(&head, 'a');
    printList(head);
    deleteAtBeginning(&head);
    printList(head);
    deleteAtBeginning(&head);
    printf("%d\n",isEmpty(head));
    return 0;
}

void insertAtBeginning(ListNodePtr *head, char value) {
    ListNodePtr new_node = malloc(sizeof(node_t));
    new_node->data = value;
    new_node->next = *head;
    *head = new_node;
}

void insertAtEnd(ListNodePtr *head, char value) {
    ListNodePtr current = *head;
    if(current != NULL) {
        while(current->next !=NULL) {
            current = current->next;
        }

        current->next = malloc(sizeof(node_t));
        current->next->data = value;
        current->next->next = NULL;
    } else {
        current = malloc(sizeof(node_t));
        current->data = value;
        current->next = NULL;
        *head = current;
    }
}

void insert(ListNodePtr *head, char value) {
    ListNodePtr newPtr;
    ListNodePtr previousPtr;
    ListNodePtr currentPtr;

    newPtr = malloc(sizeof(node_t));

    if(newPtr != NULL) {
        newPtr->data = value;
        newPtr->next = NULL;

        previousPtr = NULL;
        currentPtr = *head;

        while(currentPtr != NULL && value > currentPtr->data) {
            previousPtr = currentPtr;
            currentPtr = currentPtr->next;
        }

        if(previousPtr == NULL) {
            newPtr->next = *head;
        } else {
            previousPtr->next = newPtr;
            newPtr->next = currentPtr;
        }
    } else {
        printf("Could not allocate memory\n");
    }
}

void deleteAtBeginning(ListNodePtr *head) {
    ListNodePtr tempPtr = NULL;

    if(head == NULL) {
        return;
    } else {
        tempPtr = *head;
        *head = (*head)->next;
        free(tempPtr);
    }
}

char delete(ListNodePtr *head, char value) {
    ListNodePtr previousPtr;
    ListNodePtr currentPtr;
    ListNodePtr tempPtr;

    if (value == (*head)->data) {
        tempPtr = *head;
        *head = (*head)->next;
        free(tempPtr);
        return value;
    } else {
        
        previousPtr = *head;
        currentPtr = (*head)->next;
        
        while((currentPtr != NULL) && (currentPtr->data != value)) {
            previousPtr = currentPtr;
            currentPtr = currentPtr->next;
        }

        if (currentPtr != NULL) {
            tempPtr = currentPtr;
            previousPtr->next = currentPtr->next;
            free(tempPtr);
            return(value);
        }
    }
    return '\0';
}

void printList(ListNodePtr currentPtr) {
    if(currentPtr == NULL) {
        printf("Current list is empty\n");
    } else {
        printf("The list is:\n");
        while(currentPtr != NULL) {
            printf("%c --> ", currentPtr->data);
            currentPtr = currentPtr->next;
        }
        printf("NULL\n");
    }
}

int isEmpty(ListNodePtr head) {
    return head == NULL;
}