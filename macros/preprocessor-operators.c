#include <stdio.h>

#define STR(x) #x


#define printint(var) printf( #var " =%d\n", var)

#define TOKENCONCAT(x, y) x ## y

#define make_function( name ) int my_ ## name (int foo) { return foo; };

#define COMMAND(NAME) { #NAME, NAME ##_command }

struct command {
    char *name;
    void (*function) (void);
};

make_function(bar);
int main(void) {
    printf(STR(hello\n));
    int count = 5;
    printint(count);

    int xy = 10;

    printf("%d\n", TOKENCONCAT(x,y));
    
    printf("%d\n", my_bar(5));
    struct command commands[] = {
        COMMAND(quit),
        COMMAND(help)
    }
    return 0;
}