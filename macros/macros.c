#include <stdio.h>

#define PRNT(a,b) \
    printf("value 1 %d\n", a); \
    printf("value 2 %d\n", b);

int main(void) {

    int x =4;
    int y = 3;

    PRNT(4, 3);

    return 0;
}