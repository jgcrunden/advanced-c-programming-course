#include <stdio.h>

#define PI 3.14
#define CIRCLE_AREA(x) ((PI) * (x) * (x))

#define Warning(...) fprintf(stderr, __VA_ARGS__);

#define FOO BAR
#define BAR (12)

#define MAX(a,b) ((a > b) ? (a) : (b))

#define UPTO(i, n) for((i) = 0; (i) < (n); (i)++)

int main(void) {
    int y = 10;
    int c = 5;
    float area =  CIRCLE_AREA(c + 2);
    float area2 = CIRCLE_AREA(5);
    printf("Area is %f\n", area);
    printf("Area is %f\n", area2);

    Warning("%s: this programme is here\n", "Jason")
    printf("%d\n", FOO);
    printf("MAX is %d\n", MAX(6, 4));
    int i;
    UPTO(i, 10)
        printf("Hello\n");
    return 0;
}