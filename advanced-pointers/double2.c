#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// Very bad Jason! The pointer is pointing to int allocated on the stack. After foo returns the stack variable will go out of scope and ptr after foo will have undefined behaviour
void foo(int **ptr) {
    int a =5;
    *ptr = &a;
}

void bar(char **ptr) {
    *ptr = (void *)malloc(12 * sizeof(char));
    strcpy(*ptr, "Hello World");
}

int main() {
    int *ptr = NULL;
    ptr = (int *) malloc(sizeof(int));
    *ptr = 10;
    foo(&ptr);
    printf("%d\n", *ptr);
    char *myPtr = NULL;
    bar(&myPtr);
    printf("%s\n", myPtr);
    free(myPtr);
    return 0;
}
