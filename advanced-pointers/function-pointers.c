#include <stdio.h>

int somedisplay();

void func1 (int);
void func2 (int);

typedef void FuncType(int);

int main (void) {

    int (*func_ptr)();

    func_ptr = somedisplay;

    printf("Address of func_ptr is %p\n", func_ptr);

    (*func_ptr)();

    FuncType *func_ptr2 = NULL;

    func_ptr2 = func1;

    (*func_ptr2)(100);

    func_ptr2 = func2;

    (*func_ptr2)(200);

    return 0;
}

int somedisplay() {
    printf("Display some text\n");
    return 0;
}

void func1(int testarg) {
    printf("Value passed to func1 is %d\n", testarg);
    return;
}

void func2(int testarg) {
    printf("Value passed to func2 is %d\n", testarg);
    return;
}