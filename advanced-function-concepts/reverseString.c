#include <stdio.h>

char *reverse(char *str);

int main(void) {
    char str[100] = "Hello";
    char *rev = reverse(str);
    printf("Reverse of %s is %s\n", str, rev);
    return 0;
}

char *reverse(char *str) {
    static int i = 0;
    static char rev[100];

    if(*str) {
        reverse(str + 1);
        rev[i++] = *str;
    }
    return rev;
}