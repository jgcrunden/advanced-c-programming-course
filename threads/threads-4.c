#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>

void *hello_return(void *args) {
	char *hello = strdup("Hello world!\n");
	return (void *) hello;
}

int main(int argc, char *argv[]) {
	char *str = NULL;
	pthread_t thread;

	pthread_create(&thread, NULL, hello_return, NULL);
	pthread_join(thread, (void**) &str);
	printf("%s\n", str);

	return 0;
}
