#include <stdio.h>

// #define JUST_CHECKING
#define LIMIT 4
// #define MYOTHERDEF 2

int main(void) {
    int i;
    int total = 0;

    for(i = 0; i <= LIMIT; i++) {
        total += 2*i*i +1;

        #ifdef JUST_CHECKING
            printf("i=%d, running total = %d\n", i, total);
        #endif

    }

    printf("count total is %d\n", total);

    #if LIMIT == 4 && MYOTHERDEF == 2
        printf("They are equal\n");
    #endif
    return 0;
}