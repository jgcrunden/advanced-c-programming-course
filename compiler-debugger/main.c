#include <stdio.h>



union myUnion {
    int i;
    float j;
};

int main() {
    #ifdef MY_MACRO
        printf("Macro defined\n");
    #endif
    /* A comment */
    char c = -10;
    // printf("%d\n", c);
    printf("Hello world\n");
    return 0;
}

int myFunc() {
    return 0;
}