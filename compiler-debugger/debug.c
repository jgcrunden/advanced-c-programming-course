#include <stdio.h>
#include <stdlib.h>

int Debug = 0;
#define DEBON
#ifdef DEBON
#define DEBUG(level, fmt, ...) \
    if(Debug >= level) \
        fprintf(stderr, fmt, __VA_ARGS__)
#else
#define DEBUG(level, fmt, ...)
#endif
int process(int i, int j) {
    int val = 0;
    #ifdef DEBUG
        DEBUG(1, "process(%d, %d)\n", i, j);
    #endif
    val = i * j;

    #ifdef DEBUG
        DEBUG(1, "return %d\n", val);
    #endif
    return val;
}

int main(int argc, char*argv[]) {
    int arg1 = 0, arg2 = 0;

    if (argc > 2) {
        Debug = atoi(argv[1]);
        arg1 = atoi(argv[2]);
    }

    if (argc == 3)
        arg2 = atoi(argv[3]);

    #ifdef DEBUG
        DEBUG(3, "values are %d and %d\n", arg1, arg2);
    #endif

    printf("%d\n", process(arg1, arg2));
    return 0;
}