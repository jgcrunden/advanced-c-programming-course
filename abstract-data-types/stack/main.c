#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int data;
    struct node *link;
} node_t;

node_t *top = NULL;

void push(int data);
int isEmpty();
void display();
void pop();

int main(void) {
    push(1);
    push(2);
    push(3);
    push(4);
    
    display();

    pop();
    pop();

    display();
    return 0;

}

void push(int data) {
    node_t *temp = malloc(sizeof(node_t));
    if (temp != NULL) {
        temp->data = data;
        temp->link = top;
        top = temp;
    }
}

int isEmpty() {
    return top == NULL;
}

void pop() {
    node_t *temp;
    if(top != NULL) {
        temp = top;
        top = top->link;
        free(temp);
    }
}

void display() {
    node_t *temp;

    if(top != NULL) {
        temp = top;
        while(temp != NULL) {
            printf("%d\n", temp->data);
            temp = temp->link;
        }
    }
}



