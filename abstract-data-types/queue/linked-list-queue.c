#include <stdio.h>
#include <stdlib.h>

typedef struct QNode {
    int key;
    struct QNode *next;
} q_node;

typedef struct Queue {
    q_node *front, *rear;
} Queue;

q_node* newNode(int k) {
    q_node *temp = (q_node*)malloc(sizeof(q_node));
    temp->key = k;
    temp->next = NULL;
    return temp;
}

struct Queue* createQueue() {
    Queue *q = (Queue *)malloc(sizeof(q_node));
    q->front = q->rear = NULL;
    return q;
}

void enQueue(Queue *q, int k) {
    q_node *temp = newNode(k);
    if(q->rear == NULL) {
        q->front = q->rear = temp;
    } else {
        q->rear->next = temp;
        q->rear = temp;
    }
}

q_node *deQueue(Queue *q) {
    if(q->front == NULL) {
        return NULL;
    }

    q_node *temp = q->front;
    q->front = q->front->next;

    if(q->front == NULL)
        q->rear = NULL;
    
    return temp;
}

int main(void) {
    Queue *q = createQueue();
    enQueue(q, 1);
    enQueue(q, 2);
    enQueue(q, 3);
    deQueue(q);
    deQueue(q);
    enQueue(q, 4);
    enQueue(q, 5);

    q_node *n = deQueue(q);
    if (n != NULL) {
        printf("Dequeued item is %d\n", n->key);
    }
    return 0;
}