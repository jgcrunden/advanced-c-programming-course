#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct myStruct {
    char hello;
};

int main(void) {
    struct myStruct *test = NULL;
    test = malloc(sizeof(struct myStruct));
    test->hello = 'b';
    printf("hello value %c\n", test->hello);
    test->hello = 'c';
    printf("hello value %c\n", test->hello);

    char *myChar = malloc(2 * sizeof(char));
    *myChar = 'a';
    printf("%c\n", *myChar);
    *myChar = 'b';
    printf("%c\n", *myChar);

    char *myString  = malloc(5 * sizeof(char));
    strncpy(myString, "Hello", 5);
    printf("%s\n", myString);
    for(int i = 0; i < 100; i++) {
        printf("%d\n", *(myString+i));
    }

    // char *myString = malloc(3 * sizeof())

    // const int SIZE = 5;
    // char **people = malloc(SIZE * sizeof(char*));
    // size_t BUF_SIZE = 20;
    // for(int i = 0; i < SIZE; i++) {
    //     char *buffer = malloc(BUF_SIZE * sizeof(char));
    //     printf("Enter a name: ");
    //     getline(&buffer, &BUF_SIZE, stdin);
    //     char *buffTmp = buffer;
    //     while(*buffTmp != '\0') {
    //         if(*buffTmp == '\n')
    //             *buffTmp = '\0';
    //         buffTmp++;
    //     }
    //     *(people+i) = buffer;
    // }

    // for(int i = 0; i < SIZE; i++)
    //     printf("Name: %s\n", *(people+i));

    // for(int i = 0; i < SIZE; i++)
    //     free(*(people+i));
    // free(people);
    return 0;
}