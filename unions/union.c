#include <stdio.h>

typedef union car_union {
    int i_value;
    float f_value;
    char c_value[40];
} car;

union mixed {
    char c;
    float f;
    int i;
};

union number {
    int i;
    double j;
};

struct my_struct {
    int i;
    char j;
    double k;
};

// typedef union car carUnion;

int main(void) {
    car car1, car2, *car3;

    printf("Memory occupied by union is %lu\n", sizeof(car));
    printf("Char is %lu\n", sizeof(char));

    union {
        int myInt;
    } myUnion;

    union mixed x;
    x.f = 4.1234;
    printf("From mixed %f\n", x.f);
    printf("From mixed %d\n", x.i);

    x.i = 3;
    printf("From mixed %f\n", x.f);
    printf("From mixed %d\n", x.i);

    // assigns to first item in union
    union number value = {15};

    union number value2 = {.j = 4.32};

    struct my_struct joshsStruct = {.k= 123.3454};
    return 0;
}