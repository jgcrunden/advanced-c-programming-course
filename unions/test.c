#include <stdio.h>



struct thing {
    char *name;
    union  {
        char *sval;
    } u;
} tab[10];




int main(void) {
    char myString[] = "Hello";
    tab[0].u.sval = myString;
    struct thing *pTab = &tab[0];
    char val = *(tab[0].u.sval);
    printf("%p\n", pTab);
    char val2 = *pTab->u.sval;
    printf("%c\n", val2);
    return 0;
}