#include <stdio.h>

struct myStruct {
    int i;
} blah;

int main(void) {
    blah.i = 6;
    printf("%d\n", blah.i);
    return 0;
}